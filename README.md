# Installing requirements
`pip3 install selenium argparse`

# Usage
```
usage: scrapper.exe [-h] [--save] [--output OUTPUT] country_iso

Find top imports countries for a specified country

positional arguments:
  country_iso      Searched country ISO code

optional arguments:
  -h, --help       show this help message and exit
  --save           Enable file saving - results.csv by default
  --output OUTPUT  Output file with name
```
