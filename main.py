#!/usr/bin/env python3

from selenium import webdriver
import random
import argparse
import os
import sys
import json


# Global Variables
iso_code_url = "https://wits.worldbank.org/countryprofile-dataavailability.aspx?lang=en"
exports_url_begin = "https://wits.worldbank.org/CountryProfile/en/Country/"
exports_url_end = "/Year/2017/TradeFlow/Export/Partner/by-country/Product/Total"

if getattr(sys, 'frozen', False):
    DRIVER_BIN = os.path.join(sys._MEIPASS, './chromedriver')
else:
    DRIVER_BIN = os.path.join(os.getcwd(), './chromedriver')

# Scraper functions
def parsing_arguments():
    parser = argparse.ArgumentParser(description="Find top imports countries for a specified country")
    parser.add_argument('country_iso', help='Searched country ISO code')
    parser.add_argument("--save", help="Enable file saving - results.csv by default", action="store_true")
    parser.add_argument("--output", help="Output file with name")
    args = parser.parse_args()

    return args


def create_chrome_session(driver, url):
    driver.get(url)

    return driver


def get_iso_codes(driver):
    if not os.path.exists("utils/iso_codes.json") and not os.path.isfile("utils/iso_codes.json"):
        driver = create_chrome_session(driver, iso_code_url)

        country_iso = driver.find_element_by_id("dataCatalogMetadata")
        iso_list = country_iso.find_elements_by_xpath("tbody/*")
        iso_codes = {}
        for iso in iso_list:
            iso_codes[iso.find_element_by_xpath(".//td[1]").text] = iso.find_element_by_xpath(".//td[2]").text

        if not os.path.exists("utils"):
            os.makedirs("utils")

        file_content = json.dumps(iso_codes)
        file = open("utils/iso_codes.json", "w+")
        file.write(file_content)
        file.close()
    else:
        file = open("utils/iso_codes.json", "r+")
        iso_codes = json.loads(file.read())

    return driver, iso_codes


def get_random_iso(iso_codes):
    iso_index = []
    for code in iso_codes.keys():
        iso_index.append(iso_codes[code])

    iso_list = []
    for index in range(0, 5):
        iso_list.append(iso_index[random.randrange(len(iso_index))])

    return iso_list


def human_format(num):
    magnitude = 0
    while abs(num) >= 1000:
        magnitude += 1
        num /= 1000.0
    return '%.2f%s' % (num, ['', 'K', 'M', 'B', 'T', 'P'][magnitude])


def get_top_exports(driver, iso_code):
    driver = create_chrome_session(driver, exports_url_begin + iso_code + exports_url_end)

    country_export = driver.find_element_by_id("contenttablejqx-ProductGrid")

    if country_export.find_element_by_xpath(".//div[1]/span").text == "No data to display":
        return driver, []
    else:
        export_list = country_export.find_elements_by_xpath("*")
        exports = []
        for index in range(0, 4):
            element = {
                "country": export_list[index].find_element_by_xpath(".//a").text,
                "amount": human_format(float(export_list[index].find_element_by_xpath("div[2]/div").text.replace(',', '')) * 1000)
            }
            exports.append(element)

    return driver, exports


def format_result(iso_code, raw):
    if not raw:
        export = {
            "iso_code": iso_code,
            "data": "No data available"
        }
    else:
        export = {
            "iso_code": iso_code,
            "data": raw
        }

    return export


def print_exports(exports, iso_codes):
    for export in exports:
        if len(exports) > 1:
            print(export["iso_code"])
        if isinstance(export["data"], str):
            print(export["data"])
        else:
            for data in export["data"]:
                if data["country"] in iso_codes:
                    print("> " + iso_codes[data["country"]] + ' ' + data["amount"])
                else:
                    print("> " + data["country"] + ' ' + data["amount"])
        if exports.index(export) + 1 != len(exports):
            print()


def format_file(filename, exports, iso_codes):
    file = open(filename, "w+")

    file.write("country_iso,import_iso,import_amount\n")
    for export in exports:
        if isinstance(export["data"], str):
            file.write(export["iso_code"] + ',,\n')
        else:
            for data in export["data"]:
                if data["country"] in iso_codes:
                    file.write(export["iso_code"] + ',' + iso_codes[data["country"]] + ',' + data["amount"] + '\n')
                else:
                    file.write(export["iso_code"] + ',' + data["country"] + ',' + data["amount"] + '\n')

    file.close()


def main():
    args = parsing_arguments()

    driver = webdriver.Chrome(executable_path=DRIVER_BIN)
    driver.implicitly_wait(30)
    exports = []

    driver, iso_codes = get_iso_codes(driver)
    if args.country_iso == "all":
        countries = get_random_iso(iso_codes)
        for country in countries:
            driver, raw = get_top_exports(driver, country)
            exports.append(format_result(country, raw))
    else:
        driver, raw = get_top_exports(driver, args.country_iso)
        exports.append(format_result(args.country_iso, raw))

    if args.save and args.output:
        print("> saving results to " + args.output)
        format_file(args.output, exports, iso_codes)
    elif args.save:
        print("> saving results to results.csv")
        format_file("results.csv", exports, iso_codes)
    else:
        if args.output and not args.save:
            print("[WARNING] --output OUTPUT argument need --save in order to work. ~ Argument ignored")
        print_exports(exports, iso_codes)

if __name__ == "__main__":
    main()